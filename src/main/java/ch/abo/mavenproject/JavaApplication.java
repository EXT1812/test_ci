/*
 * Copyright (C) 2019  Abonobo SA. All rights reserved.
 */

package ch.abo.mavenproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JavaApplication {

    
    private static String DB_URL = "jdbc:mysql://localhost:3306/formation_docker";
    private static String DB_USER = "root";
    private static String DB_PASSWD = "root";
    private static String DB_TABLE = "docker_server";
    private static Connection connectionDB = null;
     
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        connectToDataBase();
        printDockerServers();
    }
    
    private static void printDockerServers() {
            Statement requestStatement = null;
            ResultSet reqresult = null;
        try {
            requestStatement = connectionDB.createStatement();
            reqresult = requestStatement.executeQuery("SELECT * FROM docker_server");
            System.out.print(reqresult.getMetaData().getColumnLabel(1));
            System.out.print("   |    ");
            System.out.println(reqresult.getMetaData().getColumnLabel(2));
            System.out.println("-----------------");
            while (reqresult.next()) {
                System.out.print(reqresult.getString(1));
                System.out.print(" | "+reqresult.getString(2));
                System.out.println();
            }
        } catch (SQLException ex) {
            Logger.getLogger(JavaApplication.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
                try {
                    reqresult.close();
                    requestStatement.close();
                    connectionDB.close();
                } catch (SQLException ex) {
                    Logger.getLogger(JavaApplication.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        
    }
    private static void connectToDataBase() {
        try {
            connectionDB=DriverManager.getConnection(DB_URL,DB_USER,DB_PASSWD);
        } catch (SQLException ex) {
            Logger.getLogger(JavaApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

